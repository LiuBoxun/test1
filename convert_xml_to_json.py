#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import xmltodict
import os


path1= os.environ.get("SOURCE_XML_FILES")
path2= os.environ.get("OUTPUT_JSON_FILES")

print("script started",path1,path2)

with open(path1) as xml_file:
    parsed_data = xmltodict.parse(xml_file.read())
    xml_file.close()
    json_conversion = json.dumps(parsed_data,indent=2)
    
    with open(path2, 'w') as json_file:
        json_file.write(json_conversion)
        json_file.close()

print("script finised")