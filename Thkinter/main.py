#!/usr/bin/python
# -*- coding: utf-8 -*-
import time
import tkinter as tk
import os

# 创建窗口
win = tk.Tk()
# 设置窗口标题
win.title('json <-> XML')

# 获取屏幕的尺寸
sw = win.winfo_screenwidth()
sh = win.winfo_screenheight()
# 设置窗口宽度和高度
win.geometry('%dx%d' % (sw,sh))
# 设置窗口的颜色
win.configure(background="gray")
# 左边输入文本
left_input = tk.Text(win,height=45,width=70)
left_input.pack(side="left")

# 右边输出文本
right_input = tk.Text(win,height=45,width=70)
right_input.pack(side="right")



def get_output_json():

# 写入xml 文件
    # 左边输入文本
    result = left_input.get("1.0","end")
    
    path1 = r'C:\Users\eboxliu\shell-test\hello\test1\Thkinter\input.xml'
    file = open(path1,"w")
    file.write(result)

# 调用convert_json_xml.py 脚本
    os.chdir(r"C:\Users\eboxliu\shell-test\hello\test1\Thkinter")
    str = ('python convert_json_xml.py')
    os.system(str)
# 右边输出json文件
    time.sleep(2)
    path2 = r'C:\Users\eboxliu\shell-test\hello\test1\Thkinter\output.json'
    with open(path2) as f:
        r2 = f.read()    
        print(r2) 
    right_input.insert("end",r2 +"\n")

def get_output_xml():
    pass
Button1=tk.Button(win,text="xml -> json",bg="green",command=get_output_json)
#Button2=tk.Button(win,text="json -> xml",bg="green",command=get_output_xml)
Button1.pack()
#Button2.pack()
# 主循环
win.mainloop()

