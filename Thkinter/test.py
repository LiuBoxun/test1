from multiprocessing import Queue
from queue import Queue
from threading import Thread,Lock
import os
import time

def generate_json_files():
    Lock_obj.acquire()
    if not q.empty():
        # print(q.get())
        filename = q.get()
        json_name = filename[:-4]
        print(json_name)
        Lock_obj.release()


def test():
    list = os.listdir(input_files)
    for i in list:
        q.put(i)
        print(i)
    # thread1 = Thread(target=generate_json_files)
    # thread1.start()
    while not q.empty():
        thread1 = Thread(target=generate_json_files)
        thread2 = Thread(target=generate_json_files)
        thread1.start()
        thread2.start()
        thread1.join()
        thread2.join()

    print(q.qsize())

if __name__ == '__main__':
    start = time.time()
    print(start)
    q = Queue()
    Lock_obj = Lock()
    pwd = os.getcwd()
    input_files = pwd +'\workspace\input_files'
    test()
    print(f'in total :{time.time()-start}')