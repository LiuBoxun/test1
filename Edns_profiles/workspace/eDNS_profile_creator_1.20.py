#!/usr/bin/python
# -*- coding: utf-8 -*-

import csv
import json
from collections import defaultdict
import re
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input_file", type = str, default = "sample.csv")
parser.add_argument("-o", "--output_file", type = str, default = "sample.json")
args = parser.parse_args()

def create_nfServiceDict(Dict):
    nfs = defaultdict(list)
    nfs['serviceName'] = str() 
    nfs['serviceInstanceId'] = str() 
    nfs['fqdn'] = str() 
    nfs['priority'] = int() 
    nfs['capacity'] = int() 
    nfs['scheme'] = str()
    nfs['nfServiceStatus'] = str()


    ip=dict()
    ip['ipv4Address'] = str()
    ip['ipv6Address'] = str()
    ip['port'] = int()
    nfs['ipEndPoints'].append(ip)


    ver=dict()
    ver['apiVersionInUri'] = str()
    ver['apiFullVersion'] = str()
    nfs['versions'].append(ver)

    Dict["nfServices"].append(nfs)
    return Dict


def Read_profile(datarow,dict01, count):
    if  datarow[1] == "serviceName":
        if datarow[5] == "n/a":
            dict01['nfServices'][count].pop('serviceName')  
        else:
            dict01['nfServices'][count]['serviceName'] = str(datarow[5])
    if  "serviceInstanceId" in datarow[1]:
        if datarow[5] == '""' or datarow[5] == 'n/a':
            dict01['nfServices'][count]['serviceInstanceId'] = str()
        else:
            dict01['nfServices'][count]['serviceInstanceId'] = str(datarow[5])
    if datarow[1] == "fqdn":
        if datarow[5] == "n/a":
            dict01['nfServices'][count].pop('fqdn')  
        else:
            dict01['nfServices'][count]['fqdn'] = str(datarow[5])

    if datarow[3]=="ipv4Address":
        if datarow[5] == "n/a":
            dict01['nfServices'][count]['ipEndPoints'][0].pop('ipv4Address')  
        else:
            dict01['nfServices'][count]['ipEndPoints'][0]['ipv4Address'] = str(datarow[5])
    if datarow[3]=="ipv6Address":
        if datarow[5] == "n/a":
            dict01['nfServices'][count]['ipEndPoints'][0].pop('ipv6Address')  
        else:
            dict01['nfServices'][count]['ipEndPoints'][0]['ipv6Address'] = str(datarow[5])
    if datarow[3]=="port":
        if datarow[5] == "n/a":
            dict01['nfServices'][count]['ipEndPoints'][0].pop('port')  
        else:
            dict01['nfServices'][count]['ipEndPoints'][0]['port'] = int(datarow[5])

    if datarow[1] == "priority":
        if datarow[5] == "n/a":
            dict01['nfServices'][count].pop('priority')  
        else:
            dict01['nfServices'][count]['priority'] = int(datarow[5])
    if datarow[1] == "capacity":
        if datarow[5] == "n/a":
            dict01['nfServices'][count].pop('capacity')  
        else:
            dict01['nfServices'][count]['capacity'] = int(datarow[5])
    if datarow[1] == "scheme":
        if datarow[5] == "n/a":
            dict01['nfServices'][count].pop('scheme')
        else:
            dict01['nfServices'][count]['scheme'] = int(datarow[5])
    if datarow[1] == "nfServiceStatus'":
        if datarow[5] == "n/a":
            dict01['nfServices'][count].pop('nfServiceStatus')  
        else:
            dict01['nfServices'][count]['nfServiceStatus'] = int(datarow[5])
    if datarow[3]=="apiVersionInUri'":
        dict01['nfServices'][count]['versions'][0]['apiVersionInUri'] = str(datarow[5])
    if datarow[3]=="apiFullVersion":
        dict01['nfServices'][count]['versions'][0]['apiFullVersion'] = str(datarow[5])

    return dict01

def create_NFInfoDict(nfName, Dict02):
    nf = defaultdict(list)
    nf['uutPolicy'] = str() 
    nf['uutFallbackPolicy'] = str() 
    nf['combinedNfFqdn'] = str() 
    nf['view'] = str() 

    nf['uutList'] = []
    nf['networkCapabilityList'] = []

    apn=dict()
    apn = {'apn' : str()}
    nf['apnInfoList'].append(apn)


    tac = {'tac' : str()}
    plmn01 = {'plmnId' : {'mcc': str(), 'mnc' : str()}}
    serviceList01 = defaultdict(list)
    instanceID = {'serviceInstanceId' : str()}
    serviceList01['serviceList'].append(instanceID)
    tacList = dict (plmn01, **tac, **serviceList01)
    nf['taiList'].append(tacList)

    tacRange = {'tacRange' : {'start' : str(), 'end' : str()}}
    plmn02 = {'plmnId' : {'mcc': str(), 'mnc' : str()}}
    serviceList02 = defaultdict(list)
    instanceID = {'serviceInstanceId' : str()}
    serviceList02['serviceList'].append(instanceID)
    tacRangeList = dict (plmn02, **tacRange, **serviceList02)
    nf['taiRangeList'].append(tacRangeList)

    gumme = {'mmeId' : str()}
    plmn03 = {'plmnId' : {'mcc': str(), 'mnc' : str()}}
    serviceList03 = defaultdict(list)
    instanceID = {'serviceInstanceId' : str()}
    serviceList03['serviceList'].append(instanceID)
    gummeList = dict (plmn03, **gumme, **serviceList03)
    nf['gummeiList'].append(gummeList)

    Dict02[nfName].update(nf)
    return Dict02


def Read_nfprofile(datarow,dict02,NF,state):
    if  datarow[1] == "uutPolicy":
        if datarow[5] == "n/a":
            dict02[str(NF)].pop('uutPolicy')  
        else:
            dict02[str(NF)]['uutPolicy'] = str(datarow[5])
    if datarow[1] == "uutFallbackPolicy":
        if datarow[5] == "n/a":
            dict02[str(NF)].pop('uutFallbackPolicy')  
        else:
            dict02[str(NF)]['uutFallbackPolicy'] = str(datarow[5])
    if datarow[1] == "combinedNfFqdn":
        if datarow[5] == "n/a":
            pass 
        else:
            dict02[str(NF)]['combinedNfFqdn'] = str(datarow[5])
    if datarow[1] == "view":
        if datarow[5] == "n/a":
            dict02[str(NF)].pop('view')  
        else:
            dict02[str(NF)]['view'] = str(datarow[5])

    if datarow[1]=="uutList":
        if datarow[5] == "n/a":
            dict02[str(NF)].pop('uutList')  
        else:
            i = datarow[5].split(',')
            for uut in i:
                dict02[str(NF)]['uutList'].append(int(uut))

    if datarow[1]=="networkCapabilityList":
        if datarow[5] == "n/a":
            dict02[str(NF)].pop('networkCapabilityList')  
        else:
            j = datarow[5].split(',')
            for ncl in j:
                k = ncl.strip('"')
                dict02[str(NF)]['networkCapabilityList'].append(k)

    if datarow[2]=="apn":
        if datarow[5] == "n/a":
            dict02[str(NF)].pop('apnInfoList')  
        else:
            s = []
            s = datarow[5].split('\n')
            for value in s:
                t= {}
                t = { 'apn' : value}
                dict02[str(NF)]['apnInfoList'].append(t)
            del dict02[str(NF)]['apnInfoList'][0]
    
    if datarow[2] =="plmnId" and state == "taiList":
            if datarow[5] == "n/a":
                dict02[str(NF)].pop('taiList')
            else:
                result = re.findall(r"\d+", datarow[5])
                dict02[str(NF)]['taiList'][0]['plmnId']['mcc'] = str(result[0])
                dict02[str(NF)]['taiList'][0]['plmnId']['mnc'] = str(result[1])

    if datarow[2] =="plmnId" and state == "taiRangeList":
            if datarow[5] == "n/a":
                dict02[str(NF)].pop('taiRangeList')
            else:
                result = re.findall(r"\d+", datarow[5])
                dict02[str(NF)]['taiRangeList'][0]['plmnId']['mcc'] = str(result[0])
                dict02[str(NF)]['taiRangeList'][0]['plmnId']['mnc'] = str(result[1])

    if datarow[2] =="plmnId" and state == "gummeiList":
            if datarow[5] == "n/a":
                dict02[str(NF)].pop('gummeiList')
            else:
                result = re.findall(r"\d+", datarow[5])
                dict02[str(NF)]['gummeiList'][0]['plmnId']['mcc'] = str(result[0])
                dict02[str(NF)]['gummeiList'][0]['plmnId']['mnc'] = str(result[1])

    if datarow[2] =="tac" and state == "taiList":
            if datarow[5] == "n/a":
                pass
            else:
                result = re.findall('[!-~]{4}', datarow[5])
                mcc = dict02[str(NF)]['taiList'][0]['plmnId']['mcc']
                mnc = dict02[str(NF)]['taiList'][0]['plmnId']['mnc']
                for i, tac in enumerate(result):
                    TAC = {'tac' : str(tac)}
                    plmn01 = {'plmnId' : {'mcc': str(mcc), 'mnc' : str(mnc)}}
                    serviceList01 = defaultdict(list)
                    instanceID = {'serviceInstanceId' : str()}
                    serviceList01['serviceList'].append(instanceID)
                    tacList = dict (plmn01, **TAC, **serviceList01)
                    dict02[str(NF)]['taiList'].append(tacList)

    if datarow[2] =="tacRange" and state == "taiRangeList":
            if datarow[5] == "n/a":
                pass
            else:
                result = re.findall('[A-Z0-9a-z]{4}', datarow[5])
                mcc = dict02[str(NF)]['taiRangeList'][0]['plmnId']['mcc']
                mnc = dict02[str(NF)]['taiRangeList'][0]['plmnId']['mnc']
                for i, tai in enumerate(result):
                    if i % 2 == 0:
                        j = int(i/2+1)
                        TAI = {'tacRange' : {'start' : str(tai) , 'end' : str()}}
                        plmn02 = {'plmnId' : {'mcc': str(mcc), 'mnc' : str(mnc)}}
                        serviceList02 = defaultdict(list)
                        instanceID = {'serviceInstanceId' : str()}
                        serviceList02['serviceList'].append(instanceID)
                        taiList = dict (plmn02, **TAI, **serviceList02)
                        dict02[str(NF)]['taiRangeList'].append(taiList)
                    else:
                        dict02[str(NF)]['taiRangeList'][j]['tacRange']['end'] = str(tai)

    if datarow[2] =="mmeId" and state == "gummeiList":
            if datarow[5] == "n/a":
                pass
            else:
                result = re.findall('[!-~]{6}', datarow[5])
                mcc = dict02[str(NF)]['gummeiList'][0]['plmnId']['mcc']
                mnc = dict02[str(NF)]['gummeiList'][0]['plmnId']['mnc']
                for i, gum in enumerate(result):
                    GUM = {'mmeId' : str(gum)}
                    plmn03 = {'plmnId' : {'mcc': str(mcc), 'mnc' : str(mnc)}}
                    serviceList03 = defaultdict(list)
                    instanceID = {'serviceInstanceId' : str()}
                    serviceList03['serviceList'].append(instanceID)
                    gumList = dict (plmn03, **GUM, **serviceList03)
                    dict02[str(NF)]['gummeiList'].append(gumList)

    if datarow[3] =="serviceInstanceId" and not state == "none":
        if datarow[5] == "n/a":
            pass
        else:
            for id in dict02[str(NF)][str(state)]:
                id['serviceList'][0]['serviceInstanceId'] = datarow[5]
            del dict02[str(NF)][str(state)][0]

    return dict02


temp = defaultdict(list)
temp02 =defaultdict(dict)
with open(args.input_file, 'r') as f:
    datalist1 = csv.reader(f,delimiter=",")
    count = 0
    temp = create_nfServiceDict(temp)
    flag = 1
    row_counter = 0
    state = "none"
    for datarow in datalist1:

        if not len(datarow[0]) ==0 and not row_counter == 0:
            if any(temp['nfServices'][count]['ipEndPoints'][0]) == False:
                temp['nfServices'][count].pop('ipEndPoints')
            if "nfServices" in datarow[0]:
                temp = create_nfServiceDict(temp)
                count = count + 1
                flag = 1
            if "pgw" in datarow[0]:
                temp02 = create_NFInfoDict("pgwInfo", temp02)
                flag = 2
            if "sgw" in datarow[0]:
                temp02 = create_NFInfoDict("sgwInfo", temp02)
                flag = 3
            if "mme" in datarow[0]:
                temp02 = create_NFInfoDict("mmeInfo", temp02)
                flag = 4
                        
        if "taiList" in datarow[1]:
            state = "taiList"
        if "taiRangeList" in datarow[1]:
            state = "taiRangeList"
        if "gumme" in datarow[1]:
            state = "gummeiList"

        if flag ==1:
            temp = Read_profile (datarow, temp, count)
        if flag ==2:
            temp02 = Read_nfprofile (datarow, temp02, "pgwInfo", state)
        if flag ==3:
            temp02 = Read_nfprofile (datarow, temp02, "sgwInfo", state)
        if flag ==4:
            temp02 = Read_nfprofile (datarow, temp02, "mmeInfo", state)

        row_counter = row_counter + 1
    if  flag == 4:
        temp02["mmeInfo"].pop('view')

    output = dict(temp, **temp02)

f_output_file = open(args.output_file, "w")
f_output_file = json.dump(output, f_output_file, indent=2, ensure_ascii=False)
