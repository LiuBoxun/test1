#!/usr/bin/python
# -*- coding: utf-8 -*-
import tkinter as tk
from tkinter import filedialog
import os
import shutil
import time


# create windows
win= tk.Tk()
# set window's title
win.title("generate eDNS profiles")
win.geometry("500x400")
win.configure(background="gray")
# 切换工作目录
pwd = os.getcwd()
print(pwd)
workspace = pwd +'\workspace'
input_files = pwd +'\workspace\input_files'
output_files = pwd + '\workspace\output_files'
os.chdir(workspace)
Text_top = tk.Text(win,height=10,width=50)
Text_top.pack(side="top")

Text_bottom = tk.Text(win,height=10,width=50)
Text_bottom.pack(side="bottom")


def get_input_files():
# 清理窗口
    Text_top.delete("1.0","end")
# 窗口选择文件   
    file_types = [('Text Files', '*.csv')]
    selectfile = filedialog.askopenfilenames(filetypes=file_types)
    print(selectfile)
    for path in selectfile:
# 上传文件
        print(path)
        shutil.copy(src=path,dst=input_files)
    list = os.listdir(input_files)
    for i in list:
        Text_top.insert("end",i+"\n")

def clean_input_files():
    Text_top.delete("1.0","end")
    ls = os.listdir(input_files)
    for i in ls:
        c_path = os.path.join(input_files, i)
        os.remove(c_path)
    Text_top.insert("end","input files have been cleaned")

def generate_json_files():
# 清理窗口  
    Text_bottom.delete("1.0","end")  
    csv_files = os.listdir(input_files)
    for i in csv_files:
        json_name = i[:-4]
        cmd = 'python eDNS_profile_creator_1.20.py -i ./input_files/'+i+' -o ./output_files/'+json_name+'.json'
        os.system(cmd)
# get execution time
        e_time = time.asctime(time.localtime(time.time()))
        Text_bottom.insert("end",e_time+"\n")

        
    list = os.listdir(output_files)
    for i in list:            
        Text_bottom.insert("end",i+"\n")
    Text_bottom.insert("end","EDNS file generated successfully")    

def clean_output_files():
    Text_bottom.delete("1.0","end")
    ls = os.listdir(output_files)
    for i in ls:
        c_path = os.path.join(output_files, i)
        os.remove(c_path)
    Text_bottom.insert("end","output files have been cleaned")


Button_input = tk.Button(win,text="upload csv files",command=get_input_files)
Button_input.place(x=125,y=170)
Button_clean_input = tk.Button(win,text="clean input files",command=clean_input_files)
Button_clean_input.place(x=275,y=170)
Button_output = tk.Button(win,text="generate josn files",command=generate_json_files)
Button_output.place(x=125,y=200)
Button_clean_output = tk.Button(win,text="clean output files",command=clean_output_files)
Button_clean_output.place(x=275,y=200)
win.mainloop()